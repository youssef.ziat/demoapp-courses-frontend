import React, { useState } from "react";
import "./App.css";
import  ListCourses  from "./components/ListCourses";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import  Course  from './components/Course';
import styled from 'styled-components';

const StyledLogo = styled.a`
    text-decoration : none ;
    color : ${props => props.count %2 ===0 ? 'red' : 'blue'};
`


function App() {
  const [count, setCount] = useState(0);

  setTimeout(()=>{setCount(1)},6000)
  
  return (
    <div className="App">
      <h2>
        <StyledLogo ref={null} count={count} href="/">
          My Courses
        </StyledLogo>
      </h2>
      <Router>
        <Switch>
          <Route exact path="/">
            <ListCourses />
          </Route>
          <Route exact path="/course/:id">
            <Course />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
