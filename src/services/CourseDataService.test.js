import CourseDataService from './CourseDataService';

describe('test CourseDataService service', () => {
     let initialList = [];
     const newCourse = {
               courseId: 1000,
               courseImageUrl: "test",
               courseName: "test",
               courseAuthor: "test",
               courseAuthorMail: "test"
          }
     beforeEach(() => {
          CourseDataService.retrieveAllCourses().then((response) => {
               initialList = response.data
          })  
     })
     it('shoud add new course', () => {    
          CourseDataService.addNewCourse(newCourse);
          CourseDataService.retrieveAllCourses().then((response) => {
                expect(response.data).toEqual(initialList.push(newCourse));
          });         
     })     
     it('should delete course', () => {
          CourseDataService.deleteCourse(1000);
          CourseDataService.retrieveAllCourses().then((response) => {
                expect(response.data).toEqual(initialList);
          });  
     })
})


