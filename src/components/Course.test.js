import { Course } from "./Course";
import 'jest-canvas-mock';
import { configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe("test Course component", () => {
     it("should render div loanding if there is not a valid id", () => {
          const wrapper = mount(<Course />);
          expect(wrapper.find('div').text()).toEqual('Loading ... ')
     })
})