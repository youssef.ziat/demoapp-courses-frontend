import { CourseCard } from "./CourseCard";
import 'jest-canvas-mock';
import { configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe("test CourseCard component", () => {
     let wrapper;
    
    beforeEach(() => {
         wrapper = mount(<CourseCard course={{
               courseId: 1000,
               courseImageUrl: "test",
               courseName: "test",
               courseAuthor: "youssef",
               courseAuthorMail: "youssef.ziat@nimbleways.com"
          } }/>);
    });

     it("should render elements", () => {
          expect(wrapper)
          expect(wrapper.find('p').at(0).text()).toEqual('Author: youssef')
          expect(wrapper.find('p').at(1).text()).toEqual('Mail: youssef.ziat@nimbleways.com')
     })
})