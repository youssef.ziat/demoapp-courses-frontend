import React from 'react';
// import "./CourseCard.css"
import styled from 'styled-components';


const StyledCoursesCard = styled.div`
     box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
     transition: 0.3s;
     width: 450px;
     &:hover {
          box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
     }
     img{
          width: 450px
     }
     div{
          padding: 2px 16px;
     }
`

export const CourseCard = ({ course }) => {
     return <StyledCoursesCard >
          <img src={course.courseImageUrl} alt="img" />
               <div>
               <h4><b>{course.courseName}</b></h4> 
               <p>Author: {course.courseAuthor}</p> 
               <p>Mail: {course.courseAuthorMail}</p>
               </div>
          </StyledCoursesCard>
}

