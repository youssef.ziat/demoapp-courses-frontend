import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import {columns,addNewCourse,updateCourse,deleteCourse } from "./ListCourses.conf"
import MaterialTable from 'material-table';
import { connect } from 'react-redux';
import { findAllCourses, addNewCourses, updateCourses, deleteCourses } from "../store/actions/actionsCreators";
import { getCoursesSelector } from "../store/selectors";

export const ListCourses = (props) => {
  let history = useHistory();

  useEffect(() => {
    props.findAllCourses();
  },[])
   const handleRowClick = (event, rowData) => {
      history.push("/course/"+rowData.courseId)
  };

  return (
    <div className="container" key={"list"}>
      <div className="container">
        <MaterialTable
            title="Courses"
            columns={columns}
            data={props.courses}
            editable={{
              onRowAdd: newData => addNewCourse(newData,props.addNewCourse,props.courses),
              onRowUpdate: (newData,oldData) => updateCourse(newData,oldData,props.updateCourse),
              onRowDelete: oldData => deleteCourse(oldData,props.deleteCourse),
                }}    
            options={{
              actionsColumnIndex: -1,
              paging : false
            }}
            onRowClick={handleRowClick}
        />
        {props.showTestElement && <MaterialTable/>}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  console.log(state)
  return {
      courses : getCoursesSelector(state)   // Use Selector
    }
}
const mapDispatchToProps = dispatch => {
  return {
    findAllCourses: () => dispatch(findAllCourses()),
    addNewCourse: (course, courses) => dispatch(addNewCourses(course, courses)),
    updateCourse: (course) => dispatch(updateCourses(course)),
    deleteCourse: (id) => dispatch(deleteCourses(id))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ListCourses)