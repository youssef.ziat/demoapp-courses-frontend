import React from 'react';
import 'jest-canvas-mock';
import { configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { ListCourses } from './ListCourses';
import MaterialTable from 'material-table';

configure({adapter: new Adapter()});

describe('<ListCourses />', () => {
    let wrapper;
    
    beforeEach(() => {
        wrapper = shallow(<ListCourses />);
    });

    it('should render <MaterialTable /> once', () => {
        expect(wrapper.find(MaterialTable)).toHaveLength(1);
    });
    it('should render <MaterialTable /> twice', () => {
        wrapper.setProps({showTestElement: true});
        expect(wrapper.find(MaterialTable)).toHaveLength(2);
    });
});