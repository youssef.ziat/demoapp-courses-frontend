import React from "react";

export const columns = [
        { title: '', field: 'courseImageUrl', render: rowData => <img src={rowData.courseImageUrl} style={{width: 40}}/> },
        { title: 'Name', field: 'courseName' },
        { title: 'Author', field: 'courseAuthor' },
        { title: 'Author Mail', field: 'courseAuthorMail'}
]
      

export const addNewCourse = (newData,addNewCourses,courses) => {
    return new Promise((resolve) => {
         console.log(newData)
         addNewCourses(newData, courses)
         resolve();
          })
}

export const updateCourse = (newData,oldData,updateCourse,resolve) => {
     return new Promise(() => {
         updateCourse(newData);
         resolve();
          })
}

export const deleteCourse = (oldData,deleteCourses,resolve) => {
     return new Promise(() => {
         deleteCourses(oldData.courseId);
         resolve();
          })
}