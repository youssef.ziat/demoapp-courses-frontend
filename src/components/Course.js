import React, {useState,useEffect} from 'react';
import { withRouter } from 'react-router-dom';
import CourseDataService from "../services/CourseDataService";
import { CourseCard } from './CourseCard/CourseCard';
import styled from 'styled-components';

const StyledCourse = styled.div`
          display : flex;
          justify-content: center;
     `


export const Course = (props) => {

     const { id } = props.match ? props.match.params : 1000;
     let [course, setData] = useState(null);
     useEffect(() => {
          CourseDataService.getCourseById(id).then((response) => {
               !course && setData(response.data);
          });
     }, [course]);
     
     return course ? <StyledCourse>
          <CourseCard course={course}/>
     </StyledCourse> : <div>Loading ... </div>
}


export default withRouter(Course);