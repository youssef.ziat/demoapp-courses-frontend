import * as actionsTypes from "../actions/actionsTypes"
const initialState = {
     courses : [] 
}

export const rootReducer = (state = initialState ,action) => {
     
     switch (action.type) {
          case actionsTypes.SAVE_COURSES: 
               return {
                    ...state,
                    courses: action.courses
               }
          case actionsTypes.ADD_COURSE_SAGA: 
               return {
                    courses: state.courses.concat(action.course)
               }
          case actionsTypes.UPDATE_COURSE_SAGA:
               let OldStateCourses = [...state.courses]
               let newCourses = [...state.courses];
               OldStateCourses.map((course, index) => course.courseId === action.course.courseId ? newCourses[index] = action.course : null);
               return {
                    courses: newCourses
               }
          case actionsTypes.DELETE_COURSE_SAGA:
               let OldStateDCourses = [...state.courses]
               let newDCourses = [...state.courses];
               OldStateDCourses.map((course, index) => course.courseId === action.id ? newDCourses.splice(index,1) : null);
               return {  
                    courses: newDCourses
               }
     }
     return state;
}