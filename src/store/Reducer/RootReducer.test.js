import {rootReducer} from './RootReducer';
import * as actionsTypes from '../actions/actionsTypes';

describe('test RootReducer', () => {
     const newCourse = {
               courseId: 1000,
               courseImageUrl: "test",
               courseName: "test",
               courseAuthor: "test",
               courseAuthorMail: "test"
          }
     it('should retrun thie initial state', () => {
          expect(rootReducer(undefined,{})).toEqual({
               courses : [] 
          })
     })
     it('should add new course to state', () => {
          expect(rootReducer({
               courses : [] 
          },{
               type: actionsTypes.ADD_COURSE_SAGA,
               course: newCourse
          })).toEqual({
               courses : [{
               courseId: 1000,
               courseImageUrl: "test",
               courseName: "test",
               courseAuthor: "test",
               courseAuthorMail: "test"
          }]
          })
     })
     it('sould delete course from state', () => {
          expect(rootReducer({
               courses : [{
               courseId: 1000,
               courseImageUrl: "test",
               courseName: "test",
               courseAuthor: "test",
               courseAuthorMail: "test"
          }]
          },{
               type: actionsTypes.DELETE_COURSE_SAGA,
               id: 1000
          })).toEqual({
               courses : [] 
          })
     })
})