import * as actionsTypes from "./actionsTypes"

export const findAllCourses = () => {
     return { 
          type: actionsTypes.FIND_ALL_COURSES
     }
}

export const addNewCourses = (course,courses) => {
     return {
          type: actionsTypes.ADD_NEW_COURSE,
          course: course,
          courses: courses
     }
}

export const updateCourses = (course) => {
     return {
          type: actionsTypes.UPDATE_COURSE,
          course: course
     }
}
export const deleteCourses = (id) => {
     return {
          type: actionsTypes.DELETE_COURSE,
          id : id
     }
}