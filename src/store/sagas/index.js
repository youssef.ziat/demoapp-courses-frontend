import {  takeEvery } from 'redux-saga/effects'
import { findAllCoursesSaga,addNewCourseSaga,updateCourseSaga,deleteCourseSaga } from "./courses"
import * as actionsTypes from "../actions/actionsTypes"


export function* watchCourses() {
     yield takeEvery(actionsTypes.FIND_ALL_COURSES, findAllCoursesSaga);
     yield takeEvery(actionsTypes.ADD_NEW_COURSE, addNewCourseSaga);
     yield takeEvery(actionsTypes.UPDATE_COURSE, updateCourseSaga);
     yield takeEvery(actionsTypes.DELETE_COURSE, deleteCourseSaga);
}