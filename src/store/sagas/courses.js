import { put } from 'redux-saga/effects'
import CourseDataService from '../../services/CourseDataService';
import * as actionsTypes from "../actions/actionsTypes";




export function* findAllCoursesSaga() {
     let courses = [];
     yield CourseDataService.retrieveAllCourses().then((response) => {
          courses = response.data
     });
     yield put({type : actionsTypes.SAVE_COURSES,courses: courses})
}

export function* addNewCourseSaga(action) {
     let newCourse = {
                ...action.course,
                courseId: action.courses.length*2+100
     }
     yield CourseDataService.addNewCourse(newCourse)
     yield put({type : actionsTypes.ADD_COURSE_SAGA,course: newCourse})
}
export function* updateCourseSaga(action) {
     yield CourseDataService.updateCourse(action.course);
     yield put({type : actionsTypes.UPDATE_COURSE_SAGA,course: action.course})
}

export function* deleteCourseSaga(action) {
     yield CourseDataService.deleteCourse(action.id)
     yield put({type: actionsTypes.DELETE_COURSE_SAGA ,id: action.id})
}